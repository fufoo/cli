package cli

import (
	"bufio"
	"io"

	"github.com/liamg/tml"
)

type HelpRequested struct {
	on *Request
}

func (h HelpRequested) isCliError() *Request {
	return h.on
}

func (h HelpRequested) Error() string {
	return "help requested"
}

func (req *Request) getVersion() string {
	if req.Command.Version != "" {
		return req.Command.Version
	}
	if req.Parent == nil {
		return ""
	}
	return req.Parent.getVersion()
}

func WriteCommandTree(out io.Writer, cmd *Command) {
	wr := bufio.NewWriter(out)
	defer wr.Flush()

	var recur func(at *Command, depth int)

	recur = func(at *Command, depth int) {
		tml.Fprintf(wr, "%*s<bold>%s</bold> - %s\n",
			depth*4,
			"",
			at.Name,
			at.Usage)
		for _, sub := range at.Subcommands {
			recur(sub, depth+1)
		}
	}

	recur(cmd, 0)
}

func WriteUsage(out io.Writer, req *Request) {
	cmd := req.Command
	wr := bufio.NewWriter(out)
	defer wr.Flush()

	tml.Fprintf(wr, "<bold>NAME</bold>\n")
	tml.Fprintf(wr, "    <bold>%s</bold> - %s\n", cmd.Name, cmd.Usage)
	tml.Fprintf(wr, "\n")

	tml.Fprintf(wr, "<bold>USAGE</bold>\n")

	tml.Fprintf(wr, "    ")

	//tml.Fprintf(wr, "<underline>%s</underline>\n", req.Command.Name)
	var walkup func(*Request)
	walkup = func(r *Request) {
		if r != nil {
			walkup(r.Parent)
			tml.Fprintf(wr, "%s ", r.Command.Name)
		}
	}

	walkup(req.Parent)

	tml.Fprintf(wr, "<bold>%s</bold>", cmd.Name)

	if len(cmd.Flags) > 0 {
		tml.Fprintf(wr, " [<italic>OPTION</italic>]...")
	}

	if argName, ok := cmd.args(); ok {
		if argName == "" {
			argName = "arg"
		}
		req := cmd.Args.required()
		tml.Fprintf(wr, " ")

		if !req {
			tml.Fprintf(wr, "[")
		}
		tml.Fprintf(wr, "<italic>%s</italic>", argName)
		if cmd.Args.multi() {
			tml.Fprintf(wr, "...")
		}
		if !req {
			tml.Fprintf(wr, "]")
		}
	}
	tml.Fprintf(wr, "\n\n")

	if v := cmd.Version; v != "" {
		tml.Fprintf(wr, "<bold>VERSION</bold>\n")
		tml.Fprintf(wr, "    %s\n", v)
		tml.Fprintf(wr, "\n")
	}

	if len(cmd.Subcommands) > 0 {
		tml.Fprintf(wr, "<bold>COMMANDS</bold>\n")
		for _, sub := range cmd.Subcommands {
			tml.Fprintf(wr, "    <bold>%s</bold> - %s\n", sub.Name, sub.Usage)
		}
	}

	if len(cmd.Flags) > 0 {
		tml.Fprintf(wr, "<bold>OPTIONS</bold>\n")
		//tml.Fprintf(wr, "\n")

		for _, flag := range cmd.Flags {
			flag.usage(wr)
		}
	}
}

func WriteError(out io.Writer, err error) {
	tml.Fprintf(out, "<red>%s</red>\n", err)
}

func WriteUsageWithError(out io.Writer, req *Request, err error) {
	WriteUsage(out, req)
	tml.Fprintf(out, "\n")
	WriteError(out, err)
}

func commonFlagUsage(wr *bufio.Writer, name []string, usage string, extra string, multi bool) {
	tml.Fprintf(wr, "    ")
	for i, variant := range name {
		if i != 0 {
			tml.Fprintf(wr, ", ")
		}
		tml.Fprintf(wr, "<bold>%s</bold>", variant)
	}
	if extra != "" {
		tml.Fprintf(wr, " <italic>%s</italic>", extra)
		if multi {
			tml.Fprintf(wr, " ...")
		}
	}

	tml.Fprintf(wr, "\n")
	tml.Fprintf(wr, "        %s\n", usage)
}

func (f *BoolFlag) usage(wr *bufio.Writer) {
	commonFlagUsage(wr, f.Name, f.Usage, "", false)
}

func (f *StringFlag) usage(wr *bufio.Writer) {
	arg := "VALUE"
	if f.ArgName != "" {
		arg = f.ArgName
	}
	commonFlagUsage(wr, f.Name, f.Usage, arg, false)
}

func (f *StringSliceFlag) usage(wr *bufio.Writer) {
	arg := "VALUE"
	if f.ArgName != "" {
		arg = f.ArgName
	}
	commonFlagUsage(wr, f.Name, f.Usage, arg, true)
}

func (f *NumberFlag) usage(wr *bufio.Writer) {
	arg := "VALUE"
	if f.ArgName != "" {
		arg = f.ArgName
	}
	commonFlagUsage(wr, f.Name, f.Usage, arg, false)
}

func (f *NumberSliceFlag) usage(wr *bufio.Writer) {
	arg := "VALUE"
	if f.ArgName != "" {
		arg = f.ArgName
	}
	commonFlagUsage(wr, f.Name, f.Usage, arg, true)
}

func (f *DurationFlag) usage(wr *bufio.Writer) {
	arg := "VALUE"
	if f.ArgName != "" {
		arg = f.ArgName
	}
	commonFlagUsage(wr, f.Name, f.Usage, arg, true)
}

package cli

import (
	"time"
)

type DurationFlag struct {
	Name         []string
	Usage        string
	Required     bool
	Env          string
	Validate     Validator
	ArgName      string
	DefaultValue time.Duration
}

func (s *DurationFlag) name() string {
	if len(s.Name) == 0 {
		return ""
	}
	return name(s.Name[0])
}

func (s *DurationFlag) Key() string {
	return s.name()
}

func (s *DurationFlag) getDefault() (any, bool) {
	if s.DefaultValue == 0 {
		return nil, false
	}
	return s.DefaultValue, true
}

func (s *DurationFlag) useDefault(in *Request) bool {
	if s.DefaultValue == 0 {
		return false
	}
	return singleSet[time.Duration](in, s.name(), s.DefaultValue)
}

func (s *DurationFlag) multi() bool {
	return false
}

func (s *DurationFlag) selectors() []string {
	return s.Name
}

func (s *DurationFlag) match(in *Request) (bool, error) {
	ok, value, rest, err := in.exactMatch(s, s.Name, in.Args)
	if err != nil {
		return false, err
	}
	if !ok {
		return false, nil
	}
	dt, err := time.ParseDuration(value)
	if err != nil {
		return false, err
	}

	if err := validateDuration(s.Validate, value, dt); err != nil {
		return false, err
	}

	return matched[time.Duration](in, s, dt, rest)
}

func (s *DurationFlag) validate(x string) error {
	dt, err := time.ParseDuration(x)
	if err != nil {
		return err
	}

	return validateDuration(s.Validate, x, dt)
}

func (s *DurationFlag) singleMatch(in *Request) (bool, error) {
	ok, value, rest, err := in.shortMatch(s, s.Name, in.Args)
	if err != nil {
		return false, err
	}
	if !ok {
		return false, nil
	}
	dt, err := time.ParseDuration(value)
	if err != nil {
		return false, err
	}
	if err := validateDuration(s.Validate, value, dt); err != nil {
		return false, err
	}

	return matched[time.Duration](in, s, dt, rest)
}

func (s *DurationFlag) required() bool {
	return s.Required
}

func validateDuration(v Validator, str string, dt time.Duration) error {
	return v.CheckString(str)
}

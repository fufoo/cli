package cli

import (
	"strconv"
)

// string

type NumberFlag struct {
	Name         []string
	Usage        string
	Required     bool
	Env          string
	Validate     Validator
	ArgName      string
	DefaultValue float64
}

func (s *NumberFlag) selectors() []string {
	return s.Name
}

// return the name of the first flag, e.g., for "--output,-o" return "output"
func (s *NumberFlag) name() string {
	if len(s.Name) == 0 {
		return ""
	}
	return name(s.Name[0])
}

func (s *NumberFlag) Key() string {
	return s.name()
}

func (s *NumberFlag) getDefault() (any, bool) {
	if s.DefaultValue == 0 {
		return nil, false
	}
	return s.DefaultValue, true
}

func (s *NumberFlag) useDefault(in *Request) bool {
	if s.DefaultValue == 0 {
		return false
	}
	return singleSet[float64](in, s.name(), s.DefaultValue)
}

func (s *NumberFlag) multi() bool {
	return false
}

func (s *NumberFlag) validate(x string) error {
	_, err := considerNumber(x, s.Validate)
	return err
}

func (s *NumberFlag) match(in *Request) (bool, error) {
	ok, value, rest, err := in.exactMatch(s, s.Name, in.Args)
	if err != nil {
		return false, err
	}
	if !ok {
		return false, nil
	}
	x, err := considerNumber(value, s.Validate)
	if err != nil {
		return false, err
	}
	return matched[float64](in, s, x, rest)
}

func (s *NumberFlag) singleMatch(in *Request) (bool, error) {
	ok, value, rest, err := in.shortMatch(s, s.Name, in.Args)
	if err != nil {
		return false, err
	}
	if !ok {
		return false, nil
	}
	x, err := considerNumber(value, s.Validate)
	if err != nil {
		return false, err
	}
	return matched[float64](in, s, x, rest)
}

func (s *NumberFlag) required() bool {
	return s.Required
}

// repeatable string

type NumberSliceFlag struct {
	Name     []string
	Usage    string
	Required bool
	Env      string
	Validate Validator
	ArgName  string
}

func (s *NumberSliceFlag) selectors() []string {
	return s.Name
}

func (s *NumberSliceFlag) name() string {
	if len(s.Name) == 0 {
		return ""
	}
	return name(s.Name[0])
}

func (s *NumberSliceFlag) Key() string {
	return s.name()
}

func (s *NumberSliceFlag) getDefault() (any, bool) {
	return nil, false
}

func considerNumber(x string, v Validator) (float64, error) {
	if v != nil {
		err := v.CheckString(x)
		if err != nil {
			return 0, err
		}
	}

	n, err := strconv.ParseFloat(x, 64)
	if err != nil {
		// TODO provide better error message
		return 0, err
	}

	if vn, ok := v.(NumberValidator); ok {
		err := vn.CheckNumber(n)
		if err != nil {
			return 0, err
		}
	}

	return n, nil
}

func (s *NumberSliceFlag) useDefault(in *Request) bool {
	return false
}

func (s *NumberSliceFlag) match(in *Request) (bool, error) {
	ok, value, rest, err := in.exactMatch(s, s.Name, in.Args)
	if err != nil {
		return false, err
	}
	if !ok {
		return false, nil
	}

	x, err := considerNumber(value, s.Validate)
	if err != nil {
		return false, err
	}

	return matched[float64](in, s, x, rest)
}

func (s *NumberSliceFlag) singleMatch(in *Request) (bool, error) {
	ok, value, rest, err := in.shortMatch(s, s.Name, in.Args)
	if err != nil {
		return false, err
	}
	if !ok {
		return false, nil
	}
	x, err := considerNumber(value, s.Validate)
	if err != nil {
		return false, err
	}
	return matched[float64](in, s, x, rest)
}

func (s *NumberSliceFlag) validate(x string) error {
	_, err := considerNumber(x, s.Validate)
	return err
}

func (s *NumberSliceFlag) multi() bool {
	return true
}

func (s *NumberSliceFlag) required() bool {
	return s.Required
}

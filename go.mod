module bitbucket.org/fufoo/cli

go 1.20

require (
	github.com/liamg/tml v0.6.0
	google.golang.org/protobuf v1.29.0
)

package main

import (
	"context"
	"fmt"
	"strings"
	"testing"

	"google.golang.org/protobuf/proto"

	"bitbucket.org/fufoo/cli"
	"bitbucket.org/fufoo/cli/protos"
	//"bitbucket.org/fufoo/cli/validate"
	"github.com/liamg/tml"
)

type capture struct {
	result string
}

type roundTripCase struct {
	name    string
	app     *cli.Command
	args    string
	success string
	failure string
}

var testapp = makeApp(captureFactory)

var cases = []roundTripCase{
	{
		name:    "subcommand",
		app:     testapp,
		args:    "left",
		success: "LEFT:90",
	},
	{
		name:    "num range validation below",
		app:     testapp,
		args:    "left -n 0",
		failure: "must be > 0",
	},
	{
		name:    "num range validation above",
		app:     testapp,
		args:    "left -n 95",
		failure: "must be <= 90",
	},
	{
		name:    "regex validation",
		app:     testapp,
		args:    "ctl color 123",
		failure: "does not match pattern",
	},
	{
		name:    "bool flag true",
		app:     testapp,
		args:    "ctl color -f blue",
		success: "COLOR:true:blue",
	},
	{
		name:    "bool flag false",
		app:     testapp,
		args:    "ctl color blue",
		success: "COLOR:-:blue",
	},
	{
		name:    "enum valid",
		app:     testapp,
		args:    "ctl action -m fast",
		success: "ACTION:fast",
	},
	{
		name:    "enum valid",
		app:     testapp,
		args:    "ctl action -m fast",
		success: "ACTION:fast",
	},
	{
		name:    "integer valid",
		app:     testapp,
		args:    "right -n 1.25",
		failure: "must be an integer",
	},
}

func TestRoundTrip(t *testing.T) {
	tml.DisableFormatting()
	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			roundtrip(t, &c)
		})
	}
}

func roundtrip(t *testing.T, c *roundTripCase) {
	cap := &capture{}
	ctx := context.WithValue(context.TODO(), "$capture", cap)

	// round trip

	// BACKEND: ENCODE
	shape, _ := protos.Export(c.app)
	wire, err := proto.Marshal(shape)
	if err != nil {
		t.Fatal(err)
	}

	// FRONTEND: DECODE
	var tmp protos.Command
	err = proto.Unmarshal(wire, &tmp)
	if err != nil {
		t.Fatal(err)
	}

	var invoked []byte
	cmd := protos.Compile(&tmp, func(inv *protos.Invocation) error {
		// FRONTEND: encode invocation
		buf, err := proto.Marshal(inv)
		if err != nil {
			panic(err)
		}
		invoked = buf
		return nil
	})

	// FRONTEND: EXECUTE

	req, err := cmd.Resolve(context.TODO(), strings.Split(c.args, " "))
	if err == nil {
		err = req.Command.Handler.Handle(req)
	}
	if err != nil {
		if c.failure == "" {
			t.Fatalf("expected command line parsing to succeed, but it failed with: %s", err)
		}
		if !strings.Contains(err.Error(), c.failure) {
			t.Fatalf("expected cli parse failure to contain %q, but it was: %s",
				c.failure,
				err.Error())
		}
		return
	}
	if c.failure != "" {
		t.Fatal("expected command line parsing to fail, but it succeeded")
	}
	// BACKEND: DECODE INVOCATION

	var inv protos.Invocation
	err = proto.Unmarshal(invoked, &inv)
	if err != nil {
		t.Fatal(err)
	}

	// BACKEND: EXECUTE INVOCATION
	err = protos.Apply(ctx, c.app, &inv)
	if err != nil {
		t.Fatal(err)
	}

	if cap.result != c.success {
		t.Errorf("expected %q but got %q", c.success, cap.result)
	}
}

type captureHandler struct {
	prefix string
	args   string
}

func (c captureHandler) Handle(r *cli.Request) error {
	cap := r.Context.Value("$capture").(*capture)
	var sb strings.Builder
	sb.WriteString(c.prefix)
	if c.args != "" {
		for i, arg := range strings.Split(c.args, ",") {
			if i > 0 {
				sb.WriteByte(',')
			}
			if r.IsSet(arg) {
				fmt.Fprintf(&sb, "%v", r.Flags[arg])
			} else {
				sb.WriteByte('-')
			}
		}
	}
	if len(r.Args) > 0 {
		sb.WriteByte(':')
		for i, arg := range r.Args {
			if i > 0 {
				sb.WriteByte(' ')
			}
			sb.WriteString(arg)
		}
	}
	cap.result = sb.String()
	return nil
}

func captureFactory(prefix, args string) cli.Handler {
	return captureHandler{
		prefix: prefix,
		args:   args,
	}
}

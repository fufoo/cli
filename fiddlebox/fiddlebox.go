package main

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"google.golang.org/protobuf/encoding/protojson"
	"google.golang.org/protobuf/proto"

	"bitbucket.org/fufoo/cli"
	"bitbucket.org/fufoo/cli/protos"
	"bitbucket.org/fufoo/cli/validate"
)

type factory func(prefix, args string) cli.Handler

func makeApp(fn factory) *cli.Command {
	return &cli.Command{
		Name:  "fiddlebox",
		Usage: "fiddle with different things",
		Subcommands: []*cli.Command{
			&cli.Command{
				Name:  "left",
				Usage: "turn left",
				Flags: []cli.Flag{
					&cli.NumberFlag{
						Name:         []string{"--amount", "-n"},
						Usage:        "amount to turn",
						DefaultValue: 90,
						ArgName:      "degrees",
						Validate: cli.AllOfValidator{
							Checks: []cli.Validator{
								validate.MustParseNumberRange("(0,90]"),
							},
						},
					},
				},
				Handler: fn("LEFT:", "amount"),
			},
			&cli.Command{
				Name:  "right",
				Usage: "turn right",
				Flags: []cli.Flag{
					&cli.NumberFlag{
						Name:         []string{"--amount", "-n"},
						Usage:        "amount to turn",
						DefaultValue: 90,
						ArgName:      "degrees",
						Validate:     validate.Integer{},
					},
				},
				Handler: fn("RIGHT:", "amount"),
			},
			&cli.Command{
				Name:  "download",
				Usage: "download a pluging",
				Args: &cli.StringFlag{
					Validate: validate.URL{},
				},
				Handler: fn("DOWNLOAD:", ""),
			},
			&cli.Command{
				Name:  "ctl",
				Usage: "control",
				Subcommands: []*cli.Command{
					&cli.Command{
						Name:    "faster",
						Usage:   "go faster",
						Handler: fn("FASTER", ""),
					},
					&cli.Command{
						Name:    "slower",
						Usage:   "go slower",
						Handler: fn("SLOWER", ""),
					},
					&cli.Command{
						Name:    "color",
						Usage:   "set color(s)",
						Handler: fn("COLOR:", "force"),
						Flags: []cli.Flag{
							&cli.BoolFlag{
								Name:  []string{"--force", "-f"},
								Usage: "force color setting",
							},
						},
						Args: &cli.StringFlag{
							Validate: validate.NewRegex(`^[a-z]+$`),
						},
					},
					&cli.Command{
						Name:  "action",
						Usage: "take action",
						Flags: []cli.Flag{
							&cli.StringFlag{
								Name:     []string{"--mode", "-m"},
								Usage:    "action mode",
								Validate: validate.NewEnum("fast", "medium", "slow"),
							},
						},
						Handler: fn("ACTION:", "mode"),
					},
				},
			},
		},
	}
}

func cliFactory(prefix, flags string) cli.Handler {
	return cli.HandlerFunc(func(r *cli.Request) error {
		fmt.Printf("%s:\n", prefix)
		if flags != "" {
			for _, flag := range strings.Split(flags, ",") {
				if r.IsSet(flag) {
					fmt.Printf("  flag %s = %v\n", flag, r.Flags[flag])
				} else {
					fmt.Printf("  flag %s is not set\n", flag)
				}
			}
		}
		if len(r.Args) > 0 {
			for i, arg := range r.Args {
				fmt.Printf("  arg[%d] = %q\n", i, arg)
			}
		}
		return nil
	})
}

var app = makeApp(cliFactory)

func main() {
	//cli.WriteCommandTree(os.Stdout, app)

	// round trip through the remoting system; pretend like we are remote
	// (the full round trip, including encoding)

	// BACKEND: ENCODE
	shape, _ := protos.Export(app)
	wire, err := proto.Marshal(shape)
	if err != nil {
		panic(err)
	}

	// FRONTEND: DECODE
	var tmp protos.Command
	err = proto.Unmarshal(wire, &tmp)
	if err != nil {
		panic(err)
	}

	var invoked []byte
	cmd := protos.Compile(&tmp, func(inv *protos.Invocation) error {
		// FRONTEND: encode invocation
		buf, err := proto.Marshal(inv)
		if err != nil {
			panic(err)
		}
		invoked = buf
		return nil
	})

	// FRONTEND: EXECUTE

	err = cmd.Run(context.TODO(), os.Args[1:])
	if err != nil {
		// parse failed
		os.Exit(1)
	}

	// BACKEND: DECODE INVOCATION

	var inv protos.Invocation
	err = proto.Unmarshal(invoked, &inv)
	if err != nil {
		panic(err)
	}

	// BACKEND: EXECUTE INVOCATION
	err = protos.Apply(context.TODO(), app, &inv)
	if err != nil {
		panic(err)
	}
}

func main1() {
	cmd := &protos.Command{
		Id:    1001,
		Name:  filepath.Base(os.Args[0]),
		Usage: "fiddle with different flags",
		Subcommands: []*protos.Command{
			&protos.Command{
				Id:    1002,
				Name:  "string",
				Usage: "fiddle with string flags",
				Flags: []*protos.Flag{
					&protos.Flag{
						Kind:       protos.Kind_STRING,
						Name:       []string{"--single", "-s"},
						Usage:      "a single required string",
						IsRequired: true,
						ArgName:    "x",
					},
					&protos.Flag{
						Kind:         protos.Kind_STRING,
						Name:         []string{"--multi", "-m"},
						Usage:        "multiple optional strings",
						IsRepeatable: true,
						ArgName:      "y",
						//Options:      []string{"non-empty"},
					},
					&protos.Flag{
						Kind:         protos.Kind_STRING,
						Name:         []string{"--another", "-a"},
						Usage:        "multiple optional strings",
						IsRepeatable: true,
						ArgName:      "z",
						//Options:      []string{"non-empty"},
					},
					&protos.Flag{
						Kind:  protos.Kind_STRING,
						Name:  []string{"--color"},
						Usage: "default value",
						DefaultValue: &protos.FlagValue{
							Value: &protos.FlagValue_Str{
								"red",
							},
						},
						//Options: []string{"enum red green blue"},
					},
				},
			},
			&protos.Command{
				Id:    1003,
				Name:  "nummer",
				Usage: "fiddle with number flags",
				Flags: []*protos.Flag{
					&protos.Flag{
						Kind:       protos.Kind_NUMBER,
						Name:       []string{"--num", "-n"},
						Usage:      "a single required number",
						IsRequired: true,
						ArgName:    "x",
					},
					&protos.Flag{
						Kind:         protos.Kind_NUMBER,
						Name:         []string{"--nonneg"},
						Usage:        "multiple non-negative integers",
						IsRepeatable: true,
						ArgName:      "y",
						//Options:      []string{">= 0", "int", "< 10"},
					},
				},
			},
			&protos.Command{
				Name:  "sub",
				Usage: "subcommands",
				Subcommands: []*protos.Command{
					&protos.Command{
						Name:  "alpha",
						Id:    1004,
						Usage: "do the alpha thing",
						Flags: []*protos.Flag{
							&protos.Flag{
								Kind:       protos.Kind_NUMBER,
								Name:       []string{"--id", "-i"},
								Usage:      "the id number",
								IsRequired: true,
								ArgName:    "k",
							},
						},
					},
					&protos.Command{
						Name:  "beta",
						Id:    1005,
						Usage: "do the beta thing",
						Flags: []*protos.Flag{
							&protos.Flag{
								Kind:       protos.Kind_STRING,
								Name:       []string{"--id", "-i"},
								Usage:      "the id string",
								IsRequired: true,
								ArgName:    "k",
							},
						},
					},
				},
			},
		},
	}
	cl := protos.Compile(cmd, func(i *protos.Invocation) error {
		fmt.Printf("invoking %q\n", i.Cmd)
		buf, err := protojson.Marshal(i)
		if err != nil {
			return err
		}
		indented, _ := json.MarshalIndent(json.RawMessage(buf), "", "  ")
		fmt.Printf("----------------\n%s\n", indented)

		for _, flag := range i.Flag {
			fmt.Printf("  %s ::= ", flag.Name)
			switch v := flag.Value.(type) {
			case *protos.FlagValue_Num:
				fmt.Printf("%g", v.Num)
			case *protos.FlagValue_Nums:
				fmt.Printf("%g", v.Nums.List)
			case *protos.FlagValue_Str:
				fmt.Printf("%q", v.Str)
			case *protos.FlagValue_Strs:
				fmt.Printf("%q", v.Strs.List)
			case *protos.FlagValue_Flag:
				fmt.Printf("%t", v.Flag)
			}
			fmt.Printf("\n")
		}
		return nil

	})
	cl.Run(context.TODO(), os.Args[1:])
}

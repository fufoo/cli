package cli

import (
	"fmt"
)

// Validator is an interface used to check argument values; the
// builtin validators are supported by the remoting protocol, but if
// your application does not do CLI remoting then you can implement
// your own Validator.
type Validator interface {
	CheckString(string) error
}

// NumberValidator is a specialized validator used by NumberFlag.
// Note that numbers passed to number flags are run through the string
// validator first, and if that passes and the validator also
// implements NumberValidator, then CheckNumber() is called on the
// parsed value.
type NumberValidator interface {
	CheckNumber(float64) error
}

type AllOfValidator struct {
	Checks []Validator
}

func (v AllOfValidator) CheckString(x string) error {
	for _, clause := range v.Checks {
		err := clause.CheckString(x)
		if err != nil {
			return err
		}
	}
	return nil
}

func (v AllOfValidator) CheckNumber(x float64) error {
	for _, clause := range v.Checks {
		if num, ok := clause.(NumberValidator); ok {
			err := num.CheckNumber(x)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func FlagErrWrap(flag string, err error) error {
	if err == nil {
		return err
	}
	return fmt.Errorf(
		"Value for flag '%s' is not valid: %w",
		flag,
		err)
}

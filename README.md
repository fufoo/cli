# Command-line Flag Parser

## Goals

* Robust
* Remotable

## Remotability

One of the design goals is that a command line interface can be
described in a form that is protobuf encodable.  This lets us build a
command-line "shell" that connects to a backend to obtain the actual
command-line user experience.

Both the description of the command and the invocation arguments can
be described in protobuf messages.

### Security

Only environment variables that are "allow-listed" by the shell will
be considered.

## Example

```
package main

import "bitbucket.org/fufoo/cli"

func main() {
    cmd := cli.Command{
        Name: "echo",
        Usage: "print a message to stdout",
        Flags: []Flag{

        },


}
```

package validate

import (
	"fmt"
	"net/url"
)

type URL struct {
}

func (URL) CheckString(x string) error {
	u, err := url.Parse(x)
	if err != nil {
		return err
	}
	if u.Scheme != "http" && u.Scheme != "https" {
		return fmt.Errorf("must be http or https URL")
	}
	return nil
}

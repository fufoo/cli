package validate

import (
	"fmt"
	"os"
)

type DirExists struct {
}

func (DirExists) CheckString(s string) error {
	sb, err := os.Stat(s)
	if err != nil {
		return err
	}
	if !sb.IsDir() {
		return fmt.Errorf("%s: not a directory", s)
	}
	return nil
}

type FileExists struct {
}

func (FileExists) CheckString(s string) error {
	sb, err := os.Stat(s)
	if err != nil {
		return err
	}
	if sb.IsDir() {
		return fmt.Errorf("%s: is a directory", s)
	}
	return nil
}

package validate

import (
	"fmt"
	"regexp"
)

type Regex struct {
	Pattern  string
	compiled *regexp.Regexp
}

func NewRegex(pattern string) Regex {
	// note, we don't really need this for performance, just to
	// catch errors early.  Since we're compiling it anyway, might
	// as well save it
	return Regex{
		Pattern:  pattern,
		compiled: regexp.MustCompile(pattern),
	}
}

func (v Regex) CheckString(s string) error {
	if v.compiled.MatchString(s) {
		return nil
	}
	return fmt.Errorf("does not match pattern %s", v.Pattern)
}

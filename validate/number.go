package validate

import (
	"fmt"
	"strconv"
	"strings"
)

type Integer struct{}

func (Integer) CheckString(x string) error {
	_, err := strconv.ParseFloat(x, 64)
	return err
}

func (Integer) CheckNumber(n float64) error {
	i := int64(n)
	if float64(i) == n {
		return nil
	}
	return fmt.Errorf("must be an integer")
}

type NumberRangeOption uint8

const (
	NumberRangeOptionHasMin     NumberRangeOption = 1 << iota
	NumberRangeOptionIncludeMin                   // closed interval end?
	NumberRangeOptionHasMax
	NumberRangeOptionIncludeMax // closed interval end?
)

type NumberRangeValidator struct {
	Options NumberRangeOption
	Min     float64
	Max     float64
}

func (v NumberRangeValidator) CheckString(x string) error {
	_, err := strconv.ParseFloat(x, 64)
	return err
}

func (v NumberRangeValidator) CheckNumber(n float64) error {
	if v.Options&NumberRangeOptionHasMin != 0 {
		if v.Options&NumberRangeOptionIncludeMin != 0 {
			if !(n >= v.Min) {
				return fmt.Errorf("must be >= %g", v.Min)
			}
		} else {
			if !(n > v.Min) {
				return fmt.Errorf("must be > %g", v.Min)
			}
		}
	}
	if v.Options&NumberRangeOptionHasMax != 0 {
		if v.Options&NumberRangeOptionIncludeMax != 0 {
			if !(n <= v.Max) {
				return fmt.Errorf("must be <= %g", v.Max)
			}
		} else {
			if !(n < v.Max) {
				return fmt.Errorf("must be < %g", v.Max)
			}
		}
	}
	return nil
}

func MustParseNumberRange(s string) NumberRangeValidator {
	nr, err := ParseNumberRange(s)
	if err != nil {
		panic(fmt.Sprintf("MustParseNumberRange(%q): %s", s, err))
	}
	return nr
}

func ParseNumberRange(s string) (r NumberRangeValidator, err error) {
	// minimum range is, e.g., (,)
	if len(s) < 3 {
		err = fmt.Errorf("invalid number range")
		return
	}

	if s[0] == '[' {
		r.Options |= NumberRangeOptionIncludeMin
	} else if s[0] == '(' {
	} else {
		err = fmt.Errorf("number range does not start with '(' or '['")
		return
	}
	s = s[1:]

	last := s[len(s)-1]

	if last == ']' {
		r.Options |= NumberRangeOptionIncludeMax
	} else if s[0] == ')' {
	} else {
		err = fmt.Errorf("number range does not end with ')' or ']'")
		return
	}

	s = s[:len(s)-1]

	parts := strings.Split(s, ",")
	if len(parts) != 2 {
		err = fmt.Errorf("number range does not contain exactly 2 values")
		return
	}

	if parts[0] != "" {
		r.Options |= NumberRangeOptionHasMin
		r.Min, err = strconv.ParseFloat(parts[0], 64)
		if err != nil {
			return
		}
	}
	if parts[1] != "" {
		r.Options |= NumberRangeOptionHasMax
		r.Max, err = strconv.ParseFloat(parts[1], 64)
		if err != nil {
			return
		}
	}
	return r, nil
}

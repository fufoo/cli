package validate

import (
	"fmt"
	"strings"
)

type Enum struct {
	Options []string
}

func NewEnum(options ...string) Enum {
	return Enum{
		Options: options,
	}
}

func (e Enum) CheckString(s string) error {
	for _, opt := range e.Options {
		if opt == s {
			return nil
		}
	}
	return fmt.Errorf("must be one of %s", orlist(e.Options))
}

func orlist(words []string) string {
	if len(words) == 2 {
		// special case if there are only two
		return fmt.Sprintf("%q or %q", words[0], words[1])
	}
	var b strings.Builder
	for i, word := range words {
		if i == len(words)-1 {
			fmt.Fprintf(&b, "or %q", word)
		} else {
			fmt.Fprintf(&b, "%q, ", word)
		}
	}
	return b.String()
}

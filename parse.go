package cli

import (
	"context"
	"fmt"
	"os"
	"strings"
	"time"
)

type Command struct {
	Name            string
	Usage           string
	Subcommands     []*Command
	Flags           []Flag
	Args            Flag
	DynamicFlags    func(*Request) []Flag
	DynamicCommands func(*Request) []*Command
	Handler         Handler
	Middleware      func(req *Request, next Handler) error
	Version         string
}

func (cmd *Command) args() (string, bool) {
	if cmd.Args == nil {
		return "", false
	}
	return cmd.Args.name(), true
}

type Handler interface {
	Handle(req *Request) error
}

type HandlerFunc func(req *Request) error

func (fn HandlerFunc) Handle(req *Request) error {
	return fn(req)
}

type Request struct {
	Parent  *Request
	Args    []string
	Context context.Context
	Command *Command
	Flags   map[string]any
}

// WithContext returns a shallow copy of r, updated to have its
// context be ctx.  The provided ctx must be non-nil.
func (r *Request) WithContext(ctx context.Context) *Request {
	dup := *r
	dup.Context = ctx
	return &dup
}

func singleSet[T any](in *Request, key string, value T) bool {
	if in.Flags == nil {
		in.Flags = make(map[string]any)
	}
	if _, ok := in.Flags[key]; ok {
		return false
	}
	in.Flags[key] = value
	return true
}

func sliceAppend[T any](in *Request, key string, value T) {
	if in.Flags == nil {
		in.Flags = make(map[string]any)
	}

	if list, ok := in.Flags[key]; ok {
		in.Flags[key] = append(list.([]T), value)
	} else {
		in.Flags[key] = []T{value}
	}
}

func matched[T any](in *Request, flag Flag, value T, rest []string) (bool, error) {
	/*if validate != nil {
		err := validate(value)
		if err != nil {
			return false, err
		}
	}*/
	if flag.multi() {
		sliceAppend[T](in, flag.name(), value)
	} else {
		if !singleSet[T](in, flag.name(), value) {
			return false, Duplicated{on: in, Arg: flag.name()}
		}
	}
	in.Args = rest
	return true, nil
}

type cliError interface {
	isCliError() *Request
}

func (e UnknownFlag) isCliError() *Request    { return e.on }
func (e UnknownCommand) isCliError() *Request { return e.on }
func (e MissingCommand) isCliError() *Request { return e.on }
func (e MissingFlag) isCliError() *Request    { return e.on }
func (e MissingValue) isCliError() *Request   { return e.on }
func (e Duplicated) isCliError() *Request     { return e.on }

type UnknownFlag struct {
	on   *Request
	Flag string
}

func (u UnknownFlag) Error() string {
	return fmt.Sprintf("flag %s is not known", u.Flag)
}

type MissingFlag struct {
	on   *Request
	flag Flag
}

func (m MissingFlag) Error() string {
	return fmt.Sprintf("flag %s is required but not specified", m.flag.name())
}

type UnknownCommand struct {
	on   *Request
	Word string
}

func (u UnknownCommand) Error() string {
	return fmt.Sprintf("unknown subcommand %q", u.Word)
}

type MissingCommand struct {
	on *Request
}

func (MissingCommand) Error() string {
	return "missing subcommand"
}

type MissingValue struct {
	on   *Request
	Flag string
}

func (m MissingValue) Error() string {
	return fmt.Sprintf("flag %q is missing a value", m.Flag)
}

type Duplicated struct {
	on  *Request
	Arg string
}

func (d Duplicated) Error() string {
	return fmt.Sprintf("argument %q duplicates previous flag", d.Arg)
}

func ShortName(s Flag) string {
	return s.name()
}

func name(s string) string {
	return strings.TrimLeft(s, "-")
}

// Run executes the command against the given arguments, reporting any
// error to stderr before returning the error.
func (cmd *Command) Run(ctx context.Context, args []string) error {
	req, err := cmd.Resolve(ctx, args)
	if err == nil {
		err = req.Command.Handler.Handle(req)
	}
	if err != nil {
		if ce, ok := err.(cliError); ok {
			re := ce.isCliError()
			if help, ok := err.(HelpRequested); ok {
				WriteUsage(os.Stderr, help.on)
			} else {
				if re != nil {
					req = re
				}
				WriteUsageWithError(os.Stderr, req, err)
			}
		} else {
			WriteError(os.Stderr, err)
		}
	}
	return err
}

// Resolve builds a Request for the given command tree with the given
// arguments, returning an error if command line parsing fails.  In
// the case of an error, Resolve may return a partially built request,
// which is useful to report usage in the correct subcommand context.
func (cmd *Command) Resolve(ctx context.Context, args []string) (*Request, error) {
	req := &Request{
		Command: cmd,
		Args:    args,
		Context: ctx,
	}
	return req.dispatch()
}

func (req *Request) dispatch() (*Request, error) {

	cmd := req.Command

	// first, parse all the flags (but only if any are defined)

	var flags []Flag
	if cmd.DynamicFlags != nil {
		flags = cmd.DynamicFlags(req)
	}
	if len(flags) > 0 {
		// the dynamic flags should take precedence
		flags = append(flags, cmd.Flags...)
	} else {
		flags = cmd.Flags
	}

	if len(flags) > 0 {
		for len(req.Args) > 0 && strings.HasPrefix(req.Args[0], "-") {
			err := req.match1(flags)
			if err != nil {
				// check for help
				if req.match1(helpFlag) == nil {
					return req, HelpRequested{req}
				}
				return req, err
			}
		}
		// make sure required flags are present, and supply
		// default values for those that are not supplied [but
		// not requried]
		for _, flag := range flags {
			name := flag.name()
			if _, ok := req.Flags[name]; !ok {
				if flag.required() {
					return req, MissingFlag{req, flag}
				} else {
					flag.useDefault(req)
				}
			}
		}
	} else {
		// check if help is requested
		if len(req.Args) > 0 && strings.HasPrefix(req.Args[0], "-") {
			if req.match1(helpFlag) == nil {
				return req, HelpRequested{req}
			}
		}
	}

	// second, figure out the subcommand if any

	var cmds []*Command
	if cmd.DynamicCommands != nil {
		cmds = cmd.DynamicCommands(req)
	}
	if len(cmds) > 0 {
		// the dynamic commands should take precedence
		cmds = append(cmds, cmd.Subcommands...)
	} else {
		cmds = cmd.Subcommands
	}

	if len(cmds) > 0 {
		// we only support exact match here
		// TODO provide approx-match help
		if len(req.Args) == 0 {
			return req, MissingCommand{req}
		}
		head := req.Args[0]
		for _, c := range cmds {
			if c.Name == head {
				if c.Middleware != nil {
					panic("TODO middleware mode")
				} /*else if c.Action != nil {
					// no middleware option here
					err := c.Action(req)
					if err != nil {
						return err
					}
				}*/
				sub := &Request{
					Parent:  req,
					Args:    req.Args[1:],
					Context: req.Context,
					Command: c,
				}
				return sub.dispatch()
			}
		}
		return req, UnknownCommand{on: req, Word: head}
	}
	if cmd.Handler == nil {
		return req, fmt.Errorf("neither handler nor subcommands!")
	}
	// validate the remaining arguments

	if len(req.Args) == 0 {
		// there are no more args left
		if cmd.Args != nil {
			// we support some... see if they are required
			if cmd.Args.required() {
				return req, fmt.Errorf("additional arguments are required but not supplied")
			}
		}
	} else {
		if cmd.Args == nil {
			return req, fmt.Errorf("no additional arguments expected")
		}

		// check them
		for _, arg := range req.Args {
			err := cmd.Args.validate(arg)
			if err != nil {
				return req, err
			}
		}
		if len(req.Args) > 1 && !cmd.Args.multi() {
			if cmd.Args.required() {
				return req, fmt.Errorf("exactly one argument expected, not %d", len(req.Args))
			} else {
				return req, fmt.Errorf("at most one argument expected, not %d", len(req.Args))
			}
		}
	}
	return req, nil
}

var helpFlag = []Flag{
	&BoolFlag{
		Name: []string{"--help", "-h"},
	},
}

func (req *Request) match1(flags []Flag) error {

	// look for an exact match
	for _, flag := range flags {
		ok, err := flag.match(req)
		if err != nil {
			return err
		}
		if ok {
			return nil
		}
	}

	// look for an inexact match
	for _, flag := range flags {
		ok, err := flag.singleMatch(req)
		if err != nil {
			return err
		}
		if ok {
			return nil
		}
	}

	return UnknownFlag{on: req, Flag: req.Args[0]}
}

// IsSet returns true if flag with the given name has been set.  In
// cases where a flag has multiple names, the name to use here is the
// first name listed in the Flag.
func (req *Request) IsSet(flag string) bool {
	for r := req; r != nil; r = r.Parent {
		_, ok := r.Flags[flag]
		if ok {
			return true
		}
	}
	return false
}

func get[T any](req *Request, flag string) (x T, present bool) {
	for r := req; r != nil; r = r.Parent {
		val, ok := r.Flags[flag]
		if ok {
			if z, ok := val.(T); ok {
				x = z
				present = true
				return
			}
			panic(fmt.Sprintf("flag %q is supplied, but is a %T not a %T", flag, val, x))
		}
	}
	present = false
	return
}

// String returns the string value of the flag with the given name.  Panics
// if the named flag is not a string flag.  (The name of a flag
// such as "--query" is "query".)
func (req *Request) String(name string) string {
	s, _ := get[string](req, name)
	return s
}

// Number returns the numeric value of the flag with the given name.
// Panics if the named flag is not a number flag.  (The name of a flag
// such as "--count" is "count".)
func (req *Request) Number(name string) float64 {
	s, _ := get[float64](req, name)
	return s
}

// Duration returns the duration value of the flag with the given
// name.  Panics if the named flag is not a duration flag.  (The name
// of a flag such as "--timeout" is "timeout".)
func (req *Request) Duration(name string) time.Duration {
	s, _ := get[time.Duration](req, name)
	return s
}

// StringSlice returns the string slice value of the flag with the
// given name.  Panics if the named flag is not a string slice flag.
// (The name of a flag such as "--user" is "user".)
func (req *Request) StringSlice(name string) []string {
	s, _ := get[[]string](req, name)
	return s
}

func (req *Request) Bool(name string) bool {
	s, _ := get[bool](req, name)
	return s
}

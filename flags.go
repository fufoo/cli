package cli

import (
	"bufio"
	"fmt"
	"strings"
)

// positional arguments are modeled using the Flag interface also
type Flag interface {
	Key() string
	selectors() []string
	name() string
	match(*Request) (bool, error)
	singleMatch(*Request) (bool, error)
	multi() bool
	usage(*bufio.Writer)
	required() bool
	useDefault(*Request) bool
	getDefault() (any, bool)
	validate(string) error
}

type FlagError struct {
	Flag    Flag
	Value   string // actual value
	Message string // error message
}

func (f FlagError) Error() string {
	sel := f.Flag.selectors()
	if len(sel) == 0 {
		return fmt.Sprintf("argument %q is not valid: %s", f.Value, f.Message)
	} else {
		return fmt.Sprintf("%q is not valid: %s", sel[0]+" "+f.Value, f.Message)
	}
}

func (in *Request) SetDefault(f Flag) {
	if data, ok := f.getDefault(); ok {
		if in.Flags == nil {
			in.Flags = make(map[string]any)
		}
		in.Flags[f.name()] = data
	}
}

type BoolFlag struct {
	Name  []string
	Usage string
	Env   string
}

func (s *BoolFlag) selectors() []string {
	return s.Name
}

func (s *BoolFlag) name() string {
	if len(s.Name) == 0 {
		return ""
	}
	return name(s.Name[0])
}

func (s *BoolFlag) Key() string {
	return s.name()
}

func (s *BoolFlag) getDefault() (any, bool) {
	return nil, false
}

func (s *BoolFlag) useDefault(in *Request) bool {
	return false
}

func (s *BoolFlag) validate(x string) error {
	// this is used for rest arg parsing
	switch x {
	case "true", "false":
		return nil
	default:
		return FlagError{
			Flag:    s,
			Value:   x,
			Message: "expected either 'true' or 'false'",
		}
	}
}

func (s *BoolFlag) match(in *Request) (bool, error) {
	for _, variant := range s.Name {
		if variant == in.Args[0] {
			return matched[bool](in, s, true, in.Args[1:])
		}
	}
	return false, nil
}

func (s *BoolFlag) singleMatch(in *Request) (bool, error) {
	args := in.Args

	for _, variant := range s.Name {
		if len(variant) == 2 && variant[0] == '-' {
			if strings.HasPrefix(args[0], variant) {
				// strip this flag off the front, so
				// if you have "-xv" and we are
				// matching "-x", then leave "-v" for
				// the next pass
				rest := []string{"-" + args[0][2:]}
				rest = append(rest, args[1:]...)
				return matched[bool](in, s, true, rest)
			}
		}
	}
	return false, nil
}

func (s *BoolFlag) multi() bool {
	return false
}

func (s *BoolFlag) required() bool {
	return false
}

// utilities for flags that take a value

func (r *Request) exactMatch(s Flag, variants []string, args []string) (bool, string, []string, error) {
	for _, variant := range variants {
		if variant == args[0] {
			if len(args) < 2 {
				return false, "", args, MissingValue{on: r, Flag: s.name()}
			}
			return true, args[1], args[2:], nil
		}
		// exact match (--flag=VALUE)
		eq := variant + "="
		if strings.HasPrefix(args[0], eq) {
			return true, args[0][len(eq):], args[1:], nil
		}
	}
	return false, "", args, nil
}

func (r *Request) shortMatch(s Flag, variants []string, args []string) (bool, string, []string, error) {
	for _, variant := range variants {
		if len(variant) == 2 && variant[0] == '-' {
			if strings.HasPrefix(args[0], variant) {
				if len(args[0]) > 2 {
					// -o/tmp/a.out
					return true, args[0][2:], args[1:], nil
				} else if len(args) < 2 {
					return false, "", args, MissingValue{on: r, Flag: s.name()}
				} else {
					return true, args[1], args[2:], nil
				}
			}
		}
	}
	return false, "", args, nil
}

// string

type StringFlag struct {
	Name     []string
	Usage    string
	Required bool
	Env      string
	Validate Validator
	// ArgName is the text to use when describing the value of the
	// flag in a usage message.
	ArgName      string
	DefaultValue string
}

// return the name of the first flag, e.g., for "--output,-o" return "output"
func (s *StringFlag) name() string {
	if len(s.Name) == 0 {
		return ""
	}
	return name(s.Name[0])
}

func (s *StringFlag) selectors() []string {
	return s.Name
}

func (s *StringFlag) Key() string {
	return s.name()
}

func (s *StringFlag) getDefault() (any, bool) {
	if s.DefaultValue == "" {
		return nil, false
	}
	return s.DefaultValue, true
}

func (s *StringFlag) useDefault(in *Request) bool {
	if s.DefaultValue == "" {
		return false
	}
	return singleSet[string](in, s.name(), s.DefaultValue)
}

func (s *StringFlag) multi() bool {
	return false
}

func (s *StringFlag) validate(x string) error {
	return validateString(s, s.Validate, x)
}

func (s *StringFlag) match(in *Request) (bool, error) {
	ok, value, rest, err := in.exactMatch(s, s.Name, in.Args)
	if err != nil {
		return false, err
	}
	if !ok {
		return false, nil
	}
	if err := validateString(s, s.Validate, value); err != nil {
		return false, err
	}
	return matched[string](in, s, value, rest)
}

func (s *StringFlag) singleMatch(in *Request) (bool, error) {
	ok, value, rest, err := in.shortMatch(s, s.Name, in.Args)
	if err != nil {
		return false, err
	}
	if !ok {
		return false, nil
	}
	if err := validateString(s, s.Validate, value); err != nil {
		return false, err
	}
	return matched[string](in, s, value, rest)
}

func (s *StringFlag) required() bool {
	return s.Required
}

// repeatable string

type StringSliceFlag struct {
	Name     []string
	Usage    string
	Required bool
	Env      string
	Validate Validator
	ArgName  string
}

func (s *StringSliceFlag) selectors() []string {
	return s.Name
}

func (s *StringSliceFlag) name() string {
	if len(s.Name) == 0 {
		return ""
	}
	return name(s.Name[0])
}

func (s *StringSliceFlag) Key() string {
	return s.name()
}

func (s *StringSliceFlag) getDefault() (any, bool) {
	return nil, false
}

func (s *StringSliceFlag) useDefault(in *Request) bool {
	return false
}

func (s *StringSliceFlag) match(in *Request) (bool, error) {
	ok, value, rest, err := in.exactMatch(s, s.Name, in.Args)
	if err != nil {
		return false, err
	}
	if !ok {
		return false, nil
	}
	if err := validateString(s, s.Validate, value); err != nil {
		return false, err
	}
	return matched[string](in, s, value, rest)
}

func (s *StringSliceFlag) validate(x string) error {
	return validateString(s, s.Validate, x)
}

func (s *StringSliceFlag) singleMatch(in *Request) (bool, error) {
	ok, value, rest, err := in.shortMatch(s, s.Name, in.Args)
	if err != nil {
		return false, err
	}
	if !ok {
		return false, nil
	}
	if err := validateString(s, s.Validate, value); err != nil {
		return false, err
	}
	return matched[string](in, s, value, rest)
}

func (s *StringSliceFlag) multi() bool {
	return true
}

func (s *StringSliceFlag) required() bool {
	return s.Required
}

func validateString(f Flag, v Validator, arg string) error {
	if v == nil {
		return nil
	}
	err := v.CheckString(arg)
	if err == nil {
		return nil
	}
	return FlagError{
		Flag:    f,
		Value:   arg,
		Message: err.Error(),
	}
}

package cli

import (
	"context"
	"strings"
	"testing"
)

func TestSubcommand(t *testing.T) {
	var alice, bob *Request

	cmd := &Command{
		Flags: []Flag{
			&StringFlag{
				Name: []string{"--foo", "-f"},
			},
		},
		Subcommands: []*Command{
			{
				Handler: HandlerFunc(func(r *Request) error {
					alice = r
					return nil
				}),
				Name: "alice",
				Flags: []Flag{
					&StringFlag{
						Name: []string{"--bar", "-b"},
					},
				},
				Args: &StringFlag{},
			},
			{
				Handler: HandlerFunc(func(r *Request) error {
					bob = r
					return nil
				}),
				Name: "bob",
				Flags: []Flag{
					&StringFlag{
						Name: []string{"--bar", "-b"},
					},
				},
				Args: &StringFlag{},
			},
		},
	}

	for _, variation := range [][]string{
		{"alice", "-bcat", "baz"},
		{"bob", "--bar", "cat", "baz"},
	} {
		err := cmd.Run(context.TODO(), variation)
		if err != nil {
			t.Fatalf("expected success, but failed with %s", err)
		}
		var r *Request

		if variation[0] == "alice" {
			if alice == nil {
				t.Fatalf("expected to capture an alice request, but didn't")
			}
			r = alice
		} else {
			if bob == nil {
				t.Fatalf("expected to capture a bob request, but didn't")
			}
			r = bob
		}
		if !r.IsSet("bar") {
			t.Fatalf("expected bar flag to be set, but isn't")
		}
		if r.String("bar") != "cat" {
			t.Fatalf("expected bar flag to be set, but isn't")
		}
		if len(r.Args) != 1 {
			t.Fatalf("expected one arg, got %d", len(r.Args))
		}
		if r.Args[0] != "baz" {
			t.Fatalf("expected one arg to be 'baz', got %q", r.Args[0])
		}
	}
}

func TestParseStringFlag(t *testing.T) {

	var req *Request

	cmd := &Command{
		Flags: []Flag{
			&StringFlag{
				Name: []string{"--foo", "-f"},
			},
		},
		Handler: HandlerFunc(func(r *Request) error {
			req = r
			return nil
		}),
	}

	for _, variation := range [][]string{
		{"-f/dev/null"},
		{"--foo", "/dev/null"},
		{"--foo=/dev/null"},
	} {
		err := cmd.Run(context.TODO(), variation)
		if err != nil {
			t.Fatalf("expected success, but failed with %s", err)
		}
		if req == nil {
			t.Fatalf("expected to capture a request, but didn't")
		}
		if v, ok := req.Flags["foo"]; ok {
			if v != "/dev/null" {
				t.Errorf("expected value of foo flag to be %q, but got %q", "/dev/null", v)
			}
		} else {
			t.Fatalf("expected to have a foo flag value, but didn't")
		}
	}
}

func TestParseBoolFlagWithArgs(t *testing.T) {

	var req *Request

	cmd := &Command{
		Flags: []Flag{
			&BoolFlag{
				Name: []string{"-q"},
			},
		},
		Handler: HandlerFunc(func(r *Request) error {
			req = r
			return nil
		}),
		Args: &StringFlag{},
	}

	for _, variation := range [][]string{
		{"-q", "hello"},
		{"hello"},
	} {
		err := cmd.Run(context.TODO(), variation)
		if err != nil {
			t.Fatalf("expected success, but failed with %s", err)
		}
		if req == nil {
			t.Fatalf("expected to capture a request, but didn't")
		}
		if len(req.Args) != 1 {
			t.Errorf("expected 1 arg, but got %d", len(req.Args))
		} else if req.Args[0] != "hello" {
			t.Fatalf("expected arg to be %q, but was %q", "hello", req.Args[0])
		}
	}
}

func TestParseMultiSingleBool(t *testing.T) {

	var req *Request

	cmd := &Command{
		Flags: []Flag{
			&BoolFlag{
				Name: []string{"--quiet", "-q"},
			},
			&BoolFlag{
				Name: []string{"--verbose", "-v"},
			},
			&BoolFlag{
				Name: []string{"-x"},
			},
		},
		Handler: HandlerFunc(func(r *Request) error {
			req = r
			return nil
		}),
	}

	for _, variation := range [][]string{
		{""},
		{"quiet", "-q"},
		{"x", "-x"},
		{"quiet x", "-qx"},
		{"verbose x", "-vx"},
		{"verbose x", "-v", "-x"},
		{"verbose x", "--verbose", "-x"},
	} {
		err := cmd.Run(context.TODO(), variation[1:])
		if err != nil {
			t.Fatalf("expected success, but failed with %s", err)
		}
		if req == nil {
			t.Fatalf("expected to capture a request, but didn't")
		}
		have := make(map[string]bool)

		if variation[0] != "" {
			for _, has := range strings.Split(variation[0], " ") {
				if !req.IsSet(has) {
					t.Errorf("expected %q to be set, but wasn't", has)
				}
				if !req.Bool(has) {
					t.Errorf("expected %q to report true, but didn't", has)
				}
				have[has] = true
			}
		}
		for _, all := range []string{"quiet", "verbose", "x"} {
			if !have[all] {
				if req.IsSet(all) {
					t.Errorf("expected %q to not be set, but was", all)
				}
				if req.Bool(all) {
					t.Errorf("expected %q to report false, but didn't", all)
				}
			}
		}
	}
}

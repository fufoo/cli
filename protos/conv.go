package protos

import (
	"context"
	"fmt"
	"sort"
	"strings"

	"bitbucket.org/fufoo/cli"
	"bitbucket.org/fufoo/cli/validate"
)

// Export takes a cli.Command structure and converts it to a
// protobuf-structured Command object that is suitable to handing to
// Compile, possibly after transport over the wire
func Export(src *cli.Command) (*Command, map[string]cli.Handler) {
	x := &exporting{
		handlers: make(map[string]cli.Handler),
	}
	top := x.export(src, nil)
	return top, x.handlers
}

type exporting struct {
	handlers map[string]cli.Handler
}

func (x *exporting) export(src *cli.Command, path []string) *Command {
	out := &Command{
		Name:    src.Name,
		Usage:   src.Usage,
		Version: src.Version,
	}
	if src.Args != nil {
		out.Rest = remoteFromLocalFlag(src.Args)
	}

	next := append(path, src.Name)
	for _, sub := range src.Subcommands {
		out.Subcommands = append(out.Subcommands, x.export(sub, next))
	}
	for _, flag := range src.Flags {
		out.Flags = append(out.Flags, remoteFromLocalFlag(flag))
	}
	if src.Handler != nil {
		x.handlers[strings.Join(path, "/")] = src.Handler
	}
	return out
}

// Apply takes a protobuf-structured invocation and executes it against
// a native command structure (cli.Command).  This is suitable for
// use in the backend which has previously exported its CLI structure,
// had that CLI structure interpreted using Compile(), had it
// invoked in Compile's callback, and the resulting Invocation handed
// back to the backend.
func Apply(ctx context.Context, dst *cli.Command, src *Invocation) error {
	if src.Cmd[0] != dst.Name {
		return fmt.Errorf("invocation is for %q but we are %q",
			src.Cmd[0],
			dst.Name)
	}

	req := dispatch(dst, nil, src.Cmd[1:])
	if req == nil {
		return fmt.Errorf("invocation %q does not match our command structure", src.Cmd)
	}

	// install the args and flags
	req.Context = ctx
	req.Args = src.Arg
	req.Flags = make(map[string]any)

	toc := make(map[string]*FlagValue)
	for _, flag := range src.Flag {
		toc[flag.Name] = flag
	}

	for _, flag := range req.Command.Flags {
		var data any
		var src *FlagValue
		name := flag.Key()
		src, ok := toc[name]
		if ok {
			switch flag := flag.(type) {
			case *cli.StringFlag:
				data = src.GetStr()
			case *cli.NumberFlag:
				data = src.GetNum()
			case *cli.BoolFlag:
				data = src.GetFlag()
			case *cli.StringSliceFlag:
				data = src.GetStrs().List
			case *cli.NumberSliceFlag:
				data = src.GetNums().List
			default:
				panic(fmt.Sprintf("TODO handle flag type %T", flag))
			}
		} else {
			// set the default if any
			req.SetDefault(flag)
		}

		if ok {
			req.Flags[name] = data
		}
	}

	if req.Command.Handler == nil {
		return fmt.Errorf("command has no handler")
	}
	return req.Command.Handler.Handle(req)
}

func dispatch(dst *cli.Command, parent *cli.Request, path []string) *cli.Request {
	this := &cli.Request{
		Parent:  parent,
		Command: dst,
	}
	if len(path) == 0 {
		return this
	}
	step := path[0]

	for _, sub := range dst.Subcommands {
		if sub.Name == step {
			return dispatch(sub, this, path[1:])
		}
	}
	// should not happen; the invocation does not match our expected structure
	return nil
}

func cmdpath(req *cli.Request) []string {
	var prev []string

	if req.Parent != nil {
		prev = cmdpath(req.Parent)
	}
	return append(prev, req.Command.Name)
}

// Compile takes a protobuf-structured command and turns it into a
// cli.Command which can then be executed; this is suitable for use in
// the front end which is receiving user input that needs to be
// reconciled against a wire-encoded command structure.  The callback
// (cb) is invoked when the returned command is executed as by
// cli.Command.Run()
func Compile(src *Command, cb func(*Invocation) error) *cli.Command {
	action := func(req *cli.Request) error {
		lst := make([]string, 0, len(req.Flags))
		for k := range req.Flags {
			lst = append(lst, k)
		}

		i := &Invocation{
			Cmd: cmdpath(req),
			Arg: req.Args,
		}

		sort.Strings(lst)
		for _, k := range lst {
			fv := &FlagValue{
				Name: k,
			}
			switch f := req.Flags[k].(type) {
			case string:
				fv.Value = &FlagValue_Str{f}
			case []string:
				fv.Value = &FlagValue_Strs{
					&StrValues{
						List: f,
					},
				}
			case float64:
				fv.Value = &FlagValue_Num{f}
			case []float64:
				fv.Value = &FlagValue_Nums{
					&NumValues{
						List: f,
					},
				}
			case bool:
				fv.Value = &FlagValue_Flag{f}
			default:
				panic(fmt.Sprintf("TODO handle flag value type %T", f))
			}
			i.Flag = append(i.Flag, fv)
		}
		return cb(i)
	}

	out := &cli.Command{
		Name:    src.Name,
		Usage:   src.Usage,
		Version: src.Version,
		Handler: cli.HandlerFunc(action),
	}
	if src.Rest != nil {
		out.Args = localFromRemoteFlag(src.Rest)
	}

	for _, flag := range src.Flags {
		x := localFromRemoteFlag(flag)
		out.Flags = append(out.Flags, x)
	}
	for _, sub := range src.Subcommands {
		x := Compile(sub, cb)
		out.Subcommands = append(out.Subcommands, x)
	}
	return out
}

func importValidation(src []*Validation) cli.Validator {
	if len(src) == 0 {
		return nil
	}
	if len(src) == 1 {
		return importOneValidation(src[0])
	}
	var lst []cli.Validator
	for _, v := range src {
		lst = append(lst, importOneValidation(v))
	}
	return cli.AllOfValidator{lst}
}

func importOneValidation(src *Validation) cli.Validator {
	switch form := src.Form.(type) {
	case *Validation_Regex:
		return validate.NewRegex(form.Regex)

	case *Validation_IsInteger:
		return validate.Integer{}

	case *Validation_Url:
		return validate.URL{}

	case *Validation_Range:
		r := form.Range
		nr := validate.NumberRangeValidator{
			Min: r.MinValue,
			Max: r.MaxValue,
		}
		if r.HasMin {
			nr.Options |= validate.NumberRangeOptionHasMin
		}
		if r.HasMax {
			nr.Options |= validate.NumberRangeOptionHasMax
		}
		if r.IncludeMin {
			nr.Options |= validate.NumberRangeOptionIncludeMin
		}
		if r.IncludeMax {
			nr.Options |= validate.NumberRangeOptionIncludeMax
		}
		return nr

	case *Validation_Enumerated:
		return validate.Enum{
			Options: form.Enumerated.Allowed,
		}
	default:
		panic(fmt.Sprintf("TODO handle import validation %T", form))
	}
}

func exportValidation(src cli.Validator) []*Validation {
	if src == nil {
		return nil
	}

	switch src := src.(type) {

	case cli.AllOfValidator:
		var lst []*Validation
		for _, clause := range src.Checks {
			lst = append(lst, exportValidation(clause)...)
		}
		return lst

	case validate.Regex:
		return []*Validation{
			&Validation{
				Form: &Validation_Regex{src.Pattern},
			},
		}

	case validate.Enum:
		return []*Validation{
			&Validation{
				Form: &Validation_Enumerated{
					&Enumeration{
						Allowed: src.Options,
					},
				},
			},
		}

	case validate.Integer:
		return []*Validation{
			&Validation{
				Form: &Validation_IsInteger{true},
			},
		}

	case validate.URL:
		return []*Validation{
			&Validation{
				Form: &Validation_Url{},
			},
		}

	case validate.NumberRangeValidator:
		nr := &NumberRange{
			MinValue:   src.Min,
			MaxValue:   src.Max,
			HasMin:     src.Options&validate.NumberRangeOptionHasMin != 0,
			HasMax:     src.Options&validate.NumberRangeOptionHasMax != 0,
			IncludeMin: src.Options&validate.NumberRangeOptionIncludeMin != 0,
			IncludeMax: src.Options&validate.NumberRangeOptionIncludeMax != 0,
		}

		return []*Validation{
			&Validation{
				Form: &Validation_Range{nr},
			},
		}
	default:
		panic(fmt.Sprintf("todo export validation %T", src))
	}
}

func remoteFromLocalFlag(src cli.Flag) *Flag {
	switch src := src.(type) {
	case *cli.BoolFlag:
		return &Flag{
			Kind:  Kind_BOOL,
			Name:  src.Name,
			Usage: src.Usage,
			Env:   src.Env,
		}

	case *cli.NumberFlag:
		return &Flag{
			Kind:        Kind_NUMBER,
			Name:        src.Name,
			Usage:       src.Usage,
			Env:         src.Env,
			ArgName:     src.ArgName,
			IsRequired:  src.Required,
			Validations: exportValidation(src.Validate),
		}

	case *cli.StringFlag:
		return &Flag{
			Kind:         Kind_STRING,
			Name:         src.Name,
			Usage:        src.Usage,
			Env:          src.Env,
			ArgName:      src.ArgName,
			IsRequired:   src.Required,
			DefaultValue: defaultStringValue(src.DefaultValue),
			Validations:  exportValidation(src.Validate),
		}

	case *cli.StringSliceFlag:
		return &Flag{
			Kind:         Kind_STRING,
			IsRepeatable: true,
			Name:         src.Name,
			Usage:        src.Usage,
			Env:          src.Env,
			ArgName:      src.ArgName,
			IsRequired:   src.Required,
			Validations:  exportValidation(src.Validate),
		}

	default:
		panic(fmt.Sprintf("TODO handle flag type %T", src))
	}
}

func defaultStringValue(x string) *FlagValue {
	if x == "" {
		return nil
	}
	return &FlagValue{
		Value: &FlagValue_Str{x},
	}
}

func localFromRemoteFlag(src *Flag) cli.Flag {
	switch src.Kind {
	case Kind_BOOL:
		return &cli.BoolFlag{
			Name:  src.Name,
			Env:   src.Env,
			Usage: src.Usage,
		}
	case Kind_NUMBER:
		val := importValidation(src.Validations)

		if src.IsRepeatable {
			return &cli.NumberSliceFlag{
				Name:     src.Name,
				Usage:    src.Usage,
				Required: src.IsRequired,
				Env:      src.Env,
				ArgName:  src.ArgName,
				Validate: val,
			}
		} else {
			return &cli.NumberFlag{
				Name:         src.Name,
				Usage:        src.Usage,
				Required:     src.IsRequired,
				Env:          src.Env,
				ArgName:      src.ArgName,
				Validate:     val,
				DefaultValue: localDefaultNum(src.DefaultValue),
			}
		}

	case Kind_STRING:
		val := importValidation(src.Validations)
		if src.IsRepeatable {
			return &cli.StringSliceFlag{
				Name:     src.Name,
				Usage:    src.Usage,
				Required: src.IsRequired,
				Env:      src.Env,
				ArgName:  src.ArgName,
				Validate: val,
			}
		} else {
			return &cli.StringFlag{
				Name:         src.Name,
				Usage:        src.Usage,
				Required:     src.IsRequired,
				Env:          src.Env,
				ArgName:      src.ArgName,
				Validate:     val,
				DefaultValue: localDefaultStr(src.DefaultValue),
			}
		}
	default:
		panic("unknown flag kind")
	}
}

func localDefaultNum(f *FlagValue) float64 {
	if f == nil {
		return 0
	}
	return f.GetNum()
}

func localDefaultStr(f *FlagValue) string {
	if f == nil {
		return ""
	}
	return f.GetStr()
}
